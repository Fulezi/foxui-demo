
#include "GL/gl3w.h"

#include <GL/gl.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#include <assert.h>
#include <stdint.h>
#include <stdio.h>

#include "Image.h"

#define STB_RECT_PACK_IMPLEMENTATION
#define STBRP_STATIC
#define STB_TRUETYPE_IMPLEMENTATION
#include "stb_rect_pack.h"
#include "stb_truetype.h"

#define SOLEIL__FOXUI_IMPLEMENTATION 1
#define FOXUI_STB_ALREADY_INCLUDED 1
/* I'm currently running on a system that won't work with version > 3.00 */
//#define SOLEIL__FOXUI_OPENGL_VERSION "#version 150\n"
#define FOXUI_PRINTERR(...) printf(__VA_ARGS__)
#include "foxui/foxui.h"

#define CHECK_OPENGL_STATE() check_opengl_state(__FILE__, __LINE__)

/** Configure user param for foxui. Implemented in foxui_configuration.c */
void foxui_configure_lnf(foxui_param *param);

/** Print any error message accumulated from OpenGL. */
void check_opengl_state(const char *file, int line);
static void glDebugOutput(GLenum source, GLenum type, GLuint id,
                          GLenum severity, GLsizei length,
                          const GLchar *message, const void *userParam);

/** We propose a number of predefined resolution to switch on. */
/* static /\*const*\/ int resolutions[][2] = {{800, 600}, {1024, 768}, {1920,
 * 1080}}; */
/* static const int number_res = FOXUI_ARRAYSIZE(resolutions); */

typedef struct {
  SDL_Window *window;
  SDL_GLContext context;
  short res;
  int res_width;
  int res_height;
  short running;
  short full_screen;
} Windows;

/** Create SDL and OpenGL context. */
int create_windows(Windows *win);
void window_events(Windows *win, foxui_renderdata *render);
void destroy_window(Windows *win);

/* TODO: Can be a foxui_ function. */
int print_image_offset(foxui_renderdata *render, const float x, const float y,
                       const int image_id, foxui_image_mode mode);
int print_image_offset_centered(foxui_renderdata *render, const float x,
                                const float y, const int image_id,
                                foxui_image_mode mode, const float ratio_x,
                                const float ratio_y);

int print_image_offset(foxui_renderdata *render, const float x, const float y,
                       const int image_id, foxui_image_mode mode) {
  foxui_rectangle offset;
  int ret;

  assert(mode == foxui_image_true_size &&
         "Other mode are not supported, current layer size is 0,0.");
  offset.rot = 0.0f;
  offset.x = x;
  offset.y = y;
  offset.width = 0.0f; /* TODO: Size will depends on mode */
  offset.height = 0.0f;
  foxui_layer_push(render, offset, NULL);
  ret = foxui_print_image(render, image_id, mode);
  foxui_layer_pop(render);
  return ret;
}

int print_image_offset_centered(foxui_renderdata *render, const float x,
                                const float y, const int image_id,
                                foxui_image_mode mode, const float ratio_x,
                                const float ratio_y) {
  foxui_rectangle offset;
  foxui_vec2 image_size;
  int ret;

  assert(mode == foxui_image_true_size &&
         "Other mode are not supported, current layer size is 0,0.");
  image_size = foxui_image_size(render, image_id);
  offset.rot = 0.0f;
  offset.x = x - (image_size.x * ratio_x);
  offset.y = y - (image_size.y * ratio_y);
  offset.width = 0.0f; /* TODO: Size will depends on mode */
  offset.height = 0.0f;
  foxui_layer_push(render, offset, NULL);
  ret = foxui_print_image(render, image_id, mode);
  foxui_layer_pop(render);
  return ret;
}

/* TODO: Easin in foxui: */
float BounceEaseOut(float p);
float BounceEaseIn(float p);
float ElasticEaseIn(float p);
float ElasticEaseOut(float p);

float BounceEaseIn(float p) { return 1 - BounceEaseOut(1 - p); }

float BounceEaseOut(float p) {
  if (p < 4 / 11.0) {
    return (121 * p * p) / 16.0;
  } else if (p < 8 / 11.0) {
    return (363 / 40.0 * p * p) - (99 / 10.0 * p) + 17 / 5.0;
  } else if (p < 9 / 10.0) {
    return (4356 / 361.0 * p * p) - (35442 / 1805.0 * p) + 16061 / 1805.0;
  } else {
    return (54 / 5.0 * p * p) - (513 / 25.0 * p) + 268 / 25.0;
  }
}

float ElasticEaseIn(float p) {
  return sin(13 * M_PI_2 * p) * pow(2, 10 * (p - 1));
}

// Modeled after the damped sine wave y = sin(-13pi/2*(x + 1))*pow(2, -10x) + 1
float ElasticEaseOut(float p) {
  return sin(-13 * M_PI_2 * (p + 1)) * pow(2, -10 * p) + 1;
}

int main(int argc, char *argv[]) {
  (void)argc;
  (void)argv;
  Windows win;
  int ret;
  char *program_status;

  /* SDL Boiler plate code */
  ret = create_windows(&win);
  if (ret == 0) {
    destroy_window(&win);
    return 1;
  }

  /* Foxui initialization. */
  foxui_renderdata render;
  foxui_param uiparam;
  Image atlas;

  bzero(&uiparam, sizeof(uiparam));
  /* TODO: Free Image */
  /* TODO: configurable asset path */
  /* TODO: Free FoxUI */
  Image_Load(&atlas, "../media/foxui_atlas.png");
  uiparam.viewport_width = win.res_width;
  uiparam.viewport_height = win.res_height;
  uiparam.atlas_texture = Image_ToGLTexture(&atlas);
  // Looks'n'Feels loading
  foxui_configure_lnf(&uiparam);

  ret = foxui_create(&uiparam, &render);
  program_status = foxui_gl_program_status(&render);
  printf("Foxui init: %s (program=%s)\n", foxui_strerror(ret), program_status);
  free(program_status);
  CHECK_OPENGL_STATE();
  if (ret != foxui_ret_success) {
    printf("Failed to init foxui");
    return 1;
  }

  const int ret_font =
      foxui_font_from_ttf_file(&uiparam, &render, "../media/HTOWERT.TTF");
  if (ret_font != foxui_ret_success) {
    printf("Failed to init foxui FONT/\n");
    return 1;
  }
  enum {
    FOXUI_DEMO_CHECK,
    FOXUI_DEMO_CHECKBOX,
    FOXUI_DEMO_CURSOR,
    FOXUI_DEMO_TEXTRIGHT,
    FOXUI_DEMO_TEXTCENTER,
    FOXUI_DEMO_TEXTLEFT,
    FOXUI_DEMO_TEXT,
    FOXUI_DEMO_SELECTOR,
    FOXUI_DEMO_BACKGROUND,
    FOXUI_DEMO_SELECTORLINE,
    FOXUI_DEMO_SELECTORDOT
  };
  while (win.running) {
    window_events(&win, &render);

    glViewport(0, 0, win.res_width, win.res_height);
    glClearColor(0.f, 0.f, 1.f, 1.f);
    glClear(GL_COLOR_BUFFER_BIT);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    render.widget_stack[0].width = win.res_width;
    render.widget_stack[0].height = win.res_height;
    render.param->viewport_width = win.res_width;
    render.param->viewport_height = win.res_height;
    /*printf("Resolution: %ix%i\n", win.res_width, win.res_height);*/

    // foxui_image_mode mode = foxui_image_fit_height;
    foxui_image_mode mode = foxui_image_true_size;

    foxui_print_image(&render, FOXUI_DEMO_BACKGROUND, mode);

    foxui_vec2 vec;
    foxui_vec2 margin;
    foxui_rectangle rec;
    rec.rot = 0.0f;
    margin.x = 0.0f;
    margin.y = 10.0f;

    // The size of the image will have the ratio of its parent.
    foxui_vec2 image_size = foxui_image_size(&render, FOXUI_DEMO_TEXT);
    vec = foxui_image_ratio(&render, FOXUI_DEMO_BACKGROUND, mode);
    rec.width = vec.x * image_size.x;
    rec.height = vec.y * image_size.y;

    // == Quest ==
    {
      int ret;
      foxui_rectangle rec_text;
      rec_text.rot = 0.0f;
      rec_text.x = 100 + rec.width * 0.25f;
      rec_text.y = 100;
      rec_text.width = 500;
      rec_text.height = 200;

      /* TODO: Bold. */
      /* TODO: Text shadow. */
      /* TODO: Text center. */
      foxui_layer_push(&render, rec_text, NULL);
      ret = foxui_write_text(&render, rec_text, "=Quest=", 100);
      foxui_layer_pop(&render);
      assert(ret == foxui_ret_success);
    }

    // Save As
    {
      rec.x = 100.0f; /* TODO: Percent relative to the parent */
      rec.y = 300.0f;

      foxui_layer_push(&render, rec, NULL);
      foxui_write_text(&render, rec, "Save As", 60);
      foxui_layer_pop(&render);
    }

    // here is the margin:
    rec.x = 350.0f; /* TODO: Percent relative to the parent */
    rec.y = 300.0f;
    // Now we push our layer relative to the parent.
    foxui_layer_push(&render, rec, NULL);
    // And print the image to fit the layer (it has the correct size).
    // foxui_print_image(&render, FOXUI_DEMO_TEXT, foxui_image_keep_ratio);
    foxui_print_image(&render, FOXUI_DEMO_TEXT, mode);
    /* TODO: Text on the left. */
    /* TODO: Extend below for other widgets */
    /* We are done for this. Pop the layer. */
    foxui_layer_pop(&render);

    /* We descend the cursor. */
    rec.y += rec.height + margin.y;
    // Fullscreen
    {
      rec.x = 100.0f; /* TODO: Percent relative to the parent */

      foxui_layer_push(&render, rec, NULL);
      foxui_write_text(&render, rec, "Fullscreen", 60);
      foxui_layer_pop(&render);
    }
    rec.x = 400.0f;
    /* Compute the size of the new image. */
    image_size = foxui_image_size(&render, FOXUI_DEMO_CHECKBOX);
    rec.width = vec.x * image_size.x;
    rec.height = vec.y * image_size.y;
    /* Push a new layer and draw. */
    // foxui_push_relative_layer(&render, rec, NULL);
    foxui_layer_push(&render, rec, NULL);
    foxui_print_image(&render, FOXUI_DEMO_CHECKBOX, mode);

    {
      foxui_rectangle rec_check;

      image_size = foxui_image_size(&render, FOXUI_DEMO_CHECK);
      rec_check.x = (rec.width - image_size.x) / 2.0f;
      rec_check.y = (rec.height - image_size.y) / 2.0f;
      rec_check.width = 10;
      rec_check.height = 10;
      rec_check.rot = 0.0f;

      static int checked = 0;
      static float checked_anim = 0.0f;

      if (foxui_layer_clicked(&render)) {
        checked = !checked;
        checked_anim = 0.0f;
      }

      if (checked) {
        foxui_rectangle rec_check_anim;
        foxui_vec2 image_size_anim;
        float ratio;

        checked_anim += 0.033f;
        checked_anim = (checked_anim <= 1.0f) ? checked_anim : 1.0f;
        ratio = ElasticEaseOut(checked_anim);

        image_size_anim.x = image_size.x * ratio;
        image_size_anim.y = image_size.y * ratio;
        rec_check_anim.x = (rec.width - image_size_anim.x) / 2.0f;
        rec_check_anim.y = (rec.height - image_size_anim.y) / 2.0f;
        rec_check_anim.width = image_size_anim.x;
        rec_check_anim.height = image_size_anim.y;
        rec_check_anim.rot = 0.0f;

        foxui_push_relative_layer(&render, rec_check_anim, NULL);
        foxui_print_image(&render, FOXUI_DEMO_CHECK, foxui_image_extend);
        foxui_layer_pop(&render);
      }
    }
    foxui_layer_pop(&render);

    /* We descend the cursor. */
    rec.y += rec.height + margin.y+ margin.y;
    // Volume
    {
      rec.x = 100.0f; /* TODO: Percent relative to the parent */
      rec.width = 250.0f;

      foxui_layer_push(&render, rec, NULL);
      foxui_write_text(&render, rec, "Volume", 60);
      foxui_layer_pop(&render);

      {
        foxui_rectangle rec_volume = rec;

	rec_volume.y -= 30;
	// 0
        rec_volume.x = 340;
        foxui_layer_push(&render, rec_volume, NULL);
        foxui_write_text(&render, rec_volume, "0", 60);
        foxui_layer_pop(&render);
	// 50
        rec_volume.x = 455;
        foxui_layer_push(&render, rec_volume, NULL);
        foxui_write_text(&render, rec_volume, "50", 60);
        foxui_layer_pop(&render);
	// 100
        rec_volume.x = 560;
        foxui_layer_push(&render, rec_volume, NULL);
        foxui_write_text(&render, rec_volume, "100", 60);
        foxui_layer_pop(&render);
      }
    }
    rec.x = 350.0f;
    rec.y += 20; /* Descend a bit the line. */

    /* Compute the size of the new image. */
    foxui_vec2 selectordot_size =
        foxui_image_size(&render, FOXUI_DEMO_SELECTORDOT);
    foxui_vec2 selectorline_size =
        foxui_image_size(&render, FOXUI_DEMO_SELECTORLINE);
    const float line_plus_dot = (selectorline_size.x + selectordot_size.x);
    rec.width = line_plus_dot;
    rec.height = selectorline_size.y + selectordot_size.y;
    /* Push a new layer and draw. */
    foxui_layer_push(&render, rec, NULL);
    /* Push all the images to make this widget. */
    {
      {
        const float x = selectordot_size.x - 1.0f;
        const float y = selectordot_size.y / 2.0f - 1.0f;

        print_image_offset(&render, x, y, FOXUI_DEMO_SELECTORLINE, mode);
      }
      foxui_print_image(&render, FOXUI_DEMO_SELECTORDOT, mode);
      print_image_offset(&render, line_plus_dot / 2.0f, 0.0f,
                         FOXUI_DEMO_SELECTORDOT, mode);
      print_image_offset(&render, line_plus_dot - 2, 0.0f,
                         FOXUI_DEMO_SELECTORDOT, mode);
    }
    static float factor = 0.0f;
    /* Push an extra layer for the drag zone. */
    /* In the following lines we divide by two some of the selector-dot size.
     * This is in order to match the cursor pointer position to the middle of
     * the selector (red) dot and the middle of the black static dots. */
    {
      foxui_rectangle drag_zone;

      /* we are inside our parent so we just need an offset of the first dot
       * size. */
      drag_zone.x = selectordot_size.x / 2.0f;
      drag_zone.y = 0.0f;
      drag_zone.width = selectorline_size.x + (selectordot_size.x);
      drag_zone.height = selectorline_size.y + selectordot_size.y;
      drag_zone.rot = 0.0f;

      foxui_layer_push(&render, drag_zone, NULL);
      if (foxui_layer_dragged(&render, &factor)) {
        /* Nothing to do but we know here that the widget was pressed. */
      }
      foxui_layer_pop(&render);

      print_image_offset_centered(
          &render, (selectordot_size.x / 2.0f) + (line_plus_dot * factor), 0.0f,
          FOXUI_DEMO_SELECTOR, mode, 0.5f, 0.0f);
    }
    foxui_layer_pop(&render);

    int ret = foxui_render(&render);
    assert(ret == 0);

    SDL_GL_SwapWindow(win.window);
  }

  destroy_window(&win);

  return 0;
}

int create_windows(Windows *win) {
  const int GL_Major = 3;
  const int GL_Minor = 3;

  win->context = NULL;
  win->window = NULL;
  win->running = 1;
  win->res = 1;
  win->full_screen = 1;
  if (SDL_Init(SDL_INIT_VIDEO) < 0) {
    printf("Failed to initialize SDL: %s\n", SDL_GetError());
    return 0;
  }

  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, GL_Major);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, GL_Minor);
  SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
  SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
  SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
  SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4);
  //
  SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 5);
  SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 5);
  SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 5);
  SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);
  SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

  Uint32 win_flags = SDL_WINDOW_OPENGL;
  if (win->full_screen) {
    win_flags |= SDL_WINDOW_FULLSCREEN_DESKTOP;
  }
  win->window =
      SDL_CreateWindow("OpenGL Test", SDL_WINDOWPOS_UNDEFINED,
                       SDL_WINDOWPOS_UNDEFINED, 1920, 1080, win_flags);
  if (win->window == NULL) {
    printf("Failed to create a new Window: %s\n", SDL_GetError());
    return 0;
  }

  win->context = SDL_GL_CreateContext(win->window);
  if (win->context == NULL) {
    printf("Failed to create a new GL Context: %s\n", SDL_GetError());
    return 0;
  }

  if (gl3wInit()) {
    printf("Failed to initialize GL3W\n");
    return 0;
  }
  if (!gl3wIsSupported(GL_Major, GL_Minor)) {
    printf("OpenGL %i.%i not supported", GL_Major, GL_Minor);
    return 0;
  }
  printf("OpenGL %s, GLSL %s.\n", glGetString(GL_VERSION),
         glGetString(GL_SHADING_LANGUAGE_VERSION));

  /* TODO: Allow enable/disable VSYNC. */
  /*  SDL_GL_SetSwapInterval(1); */

  GLint flags;
  glGetIntegerv(GL_CONTEXT_FLAGS, &flags);
  if (flags & GL_CONTEXT_FLAG_DEBUG_BIT) {
    glEnable(GL_DEBUG_OUTPUT);
    glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
    glDebugMessageCallback(glDebugOutput, NULL);
    glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, NULL,
                          GL_TRUE);
  }
  CHECK_OPENGL_STATE();

  SDL_DisplayMode mode;
  SDL_GetCurrentDisplayMode(0, &mode);
  win->res_width = mode.w;
  win->res_height = mode.h;

  return 1;
}

void destroy_window(Windows *win) {
  if (win->context) {
    SDL_GL_DeleteContext(win->context);
    win->context = NULL;
  }
  if (win->window) {
    SDL_DestroyWindow(win->window);
    win->window = NULL;
  }
  SDL_Quit();
}

void window_events(Windows *win, foxui_renderdata *render) {
  SDL_Event Event;

  if (render->primary_button == foxui_button_just_pressed) {
    render->primary_button = foxui_button_pressed;
  }
  if (render->primary_button == foxui_button_just_released) {
    render->primary_button = foxui_button_released;
  }
  while (SDL_PollEvent(&Event)) {
    if (Event.type == SDL_KEYDOWN) {
      switch (Event.key.keysym.sym) {
      case SDLK_ESCAPE:
        win->running = 0;
        break;
      case 'f':
        win->full_screen = !win->full_screen;
        if (win->full_screen) {
          SDL_SetWindowFullscreen(
              win->window, SDL_WINDOW_OPENGL | SDL_WINDOW_FULLSCREEN_DESKTOP);
        } else {
          SDL_SetWindowFullscreen(win->window, SDL_WINDOW_OPENGL);
        }
        break;
      case 'r':
        if (win->full_screen) {
          SDL_DisplayMode mode;

          static int display_in_use = 0; /* Only using first display */
          int i, display_mode_count;
          Uint32 f;

          SDL_Log("SDL_GetNumVideoDisplays(): %i", SDL_GetNumVideoDisplays());

          display_mode_count = SDL_GetNumDisplayModes(display_in_use);
          if (display_mode_count < 1) {
            SDL_Log("SDL_GetNumDisplayModes failed: %s", SDL_GetError());
            break;
          }
          SDL_Log("SDL_GetNumDisplayModes: %i", display_mode_count);

          for (i = 0; i < display_mode_count; ++i) {
            if (SDL_GetDisplayMode(display_in_use, i, &mode) != 0) {
              SDL_Log("SDL_GetDisplayMode failed: %s", SDL_GetError());
              break;
            }
            f = mode.format;

            SDL_Log("Mode %i\tbpp %i\t%s\t%i x %i", i, SDL_BITSPERPIXEL(f),
                    SDL_GetPixelFormatName(f), mode.w, mode.h);
          }

          win->res++;
          if (win->res >= display_mode_count) {
            win->res = 0;
          }

          SDL_GetDisplayMode(display_in_use, win->res, &mode);
          win->res_width = mode.w;
          win->res_height = mode.h;
          SDL_SetWindowDisplayMode(win->window, &mode);
        } else {
          /* SDL_SetWindowSize(win->window, resolutions[win->res][0], */
          /*                   resolutions[win->res][1]); */
        }
      default:
        break;
      }
    } else if (Event.type == SDL_QUIT) {
      win->running = 0;
    } else if (Event.type == SDL_MOUSEBUTTONDOWN) {
      if (Event.button.button == SDL_BUTTON_LEFT) {
        render->primary_button = foxui_button_just_pressed;
      }
    } else if (Event.type == SDL_MOUSEBUTTONUP) {
      if (Event.button.button == SDL_BUTTON_LEFT) {
        render->primary_button = foxui_button_just_released;
      }
    } else if (Event.type == SDL_MOUSEMOTION) {
      render->mouse_pos.x = Event.motion.x;
      render->mouse_pos.y = Event.motion.y;
    }
  }
}

void check_opengl_state(const char *file, int line) {
  GLenum err = GL_NO_ERROR;

  while ((err = glGetError()) != GL_NO_ERROR) {
    const char *error;
    switch (err) {
    case GL_INVALID_OPERATION:
      error = "INVALID_OPERATION";
      break;
    case GL_INVALID_ENUM:
      error = "INVALID_ENUM";
      break;
    case GL_INVALID_VALUE:
      error = "INVALID_VALUE";
      break;
    case GL_OUT_OF_MEMORY:
      error = "OUT_OF_MEMORY";
      break;
    case GL_INVALID_FRAMEBUFFER_OPERATION:
      error = "INVALID_FRAMEBUFFER_OPERATION";
      break;
    default:
      error = "Unknown"; // TODO chek all errors
    }
    printf("GL Error, %s(%i): %s:%i\n", error, err, file, line);
  }
}

static void /*APIENTRY*/
glDebugOutput(GLenum source, GLenum type, GLuint id, GLenum severity,
              GLsizei length, const GLchar *message, const void *userParam) {
  /* const size_t msg_max_length = 4095; */
  /* char message[msg_max_length + 1] = {0}; */
  const char *severitystr;
  const char *sourcestr;
  const char *typestr;

  switch (severity) {
  case GL_DEBUG_SEVERITY_LOW:
    severitystr = "[low]";
    break;
  case GL_DEBUG_SEVERITY_NOTIFICATION:
    severitystr = "[message]";
    break;
  case GL_DEBUG_SEVERITY_MEDIUM:
    severitystr = "[medium]";
    break;
  case GL_DEBUG_SEVERITY_HIGH:
    severitystr = "[HIGH]";
    break;
  }

  switch (source) {
  case GL_DEBUG_SOURCE_API:
    sourcestr = "Source: API";
    break;
  case GL_DEBUG_SOURCE_WINDOW_SYSTEM:
    sourcestr = "Source: Window System";
    break;
  case GL_DEBUG_SOURCE_SHADER_COMPILER:
    sourcestr = "Source: Shader Compiler";
    break;
  case GL_DEBUG_SOURCE_THIRD_PARTY:
    sourcestr = "Source: Third Party";
    break;
  case GL_DEBUG_SOURCE_APPLICATION:
    sourcestr = "Source: Application";
    break;
  case GL_DEBUG_SOURCE_OTHER:
    sourcestr = "Source: Other";
    break;
  }

  switch (type) {
  case GL_DEBUG_TYPE_ERROR:
    typestr = "Type: Error\n";
    break;
  case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
    typestr = "\nType: Deprecated Behaviour";
    break;
  case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
    typestr = "\nType: Undefined Behaviour";
    break;
  case GL_DEBUG_TYPE_PORTABILITY:
    typestr = "\nType: Portability";
    break;
  case GL_DEBUG_TYPE_PERFORMANCE:
    typestr = "\nType: Performance";
    break;
  case GL_DEBUG_TYPE_MARKER:
    typestr = "\nType: Marker\n";
    break;
  case GL_DEBUG_TYPE_PUSH_GROUP:
    typestr = "\nType: Push Group";
    break;
  case GL_DEBUG_TYPE_POP_GROUP:
    typestr = "\nType: Pop Group";
    break;
  case GL_DEBUG_TYPE_OTHER:
    typestr = "\nType: Other\n";
    break;
  }

  printf("%s(%s): %s%s", severitystr, sourcestr, message, typestr);
}
