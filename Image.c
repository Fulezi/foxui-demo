
#include "Image.h"

#include "GL/gl3w.h"
#include <GL/gl.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

/* TODO: To be put in a dedicated library */

int Image_Load(Image *image, const char *path) {
  if (image == NULL) {
    printf("Cannot pass NULL as image parameter");
    return -1;
  }

  // TODO: Image_Copy seems not to work with different channels
  const int number_channels = 0;
  if (number_channels > 0) {
    image->data = stbi_load(path, &(image->width), &(image->height),
                            &(image->channels), 4);
    image->channels = 4;
  } else {
    image->data = stbi_load(path, &(image->width), &(image->height),
                            &(image->channels), 0);
  }
  if (image->data == NULL) {
    // TODO: error management from STB
    printf("Failed to open image: '%s'.\n", path);
  }
  return 0;
}

GLuint Image_ToGLTexture(Image *image) {
  GLenum format;
  GLuint tex = 0;

  if (image == NULL) {
    printf("Cannot pass NULL as image parameter");
    return 0;
  }
  switch (image->channels) {
  case 1:
    format = GL_RED;
    break;
  case 2:
    format = GL_RG;
    break;
  case 3:
    format = GL_RGB;
    break;
  case 4:
    format = GL_RGBA;
    break;
  }

  glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
  glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);
  glPixelStorei(GL_UNPACK_SKIP_PIXELS, 0);
  glPixelStorei(GL_UNPACK_SKIP_ROWS, 0);

  glGenTextures(1, &tex);
  if (tex == 0) {
    // TODO: Error management from OpenGL
    return 0;
  }
  glBindTexture(GL_TEXTURE_2D, tex);
  glTexImage2D(GL_TEXTURE_2D, 0, format, image->width, image->height, 0, format,
               GL_UNSIGNED_BYTE, image->data);

  // TODO: Here check for opengl log.

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
                  GL_LINEAR_MIPMAP_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);

  return tex;
}

int Image_CreateRGBA(Image *image, const int width, const int height) {
  const int length = width * height * 4;
  const size_t size = sizeof(stbi_uc) * length;

  if (image == NULL) {
    printf("Image parameter cannot be NULL");
    return -1;
  }
  image->width = width;
  image->height = height;
  image->channels = 4;
  image->data = (stbi_uc *)malloc(size);
  if (image->data == NULL) {
    printf("Memory allocation failed");
    return -1;
  }
  memset(image->data, 0x00, length);
  return 0;
}

/*
int Image_Copy(Image *source, foxui_rectangle *source_rec, Image *dest,
               foxui_rectangle *dest_rec) {
  foxui_rectangle source_rec_full;

  if (source == NULL || dest == NULL || dest_rec == NULL) {
    printf("Parameters images source and dest along with des rectangle cannot "
           "be null");
    return -1;
  }
  if (source_rec == NULL) {
    source_rec_full.x = 0;
    source_rec_full.y = 0;
    source_rec_full.width = source->width;
    source_rec_full.height = source->height;
    source_rec = &source_rec_full;
  }
  if (source_rec->x + source_rec->width > source->width ||
      source_rec->y + source_rec->height > source->height ||
      dest_rec->x + dest_rec->width > dest->width ||
      dest_rec->y + dest_rec->height > dest->height) {
    printf("Source or Destination image will be have copied part outside of "
           "their range.\n");
    return -1;
  }
  for (int y = 0; y < source_rec->height; ++y) {
    if (y >= dest_rec->height) {
      break;
    }
    for (int x = 0; x < source_rec->width; ++x) {
      const int src_x = source_rec->x + x;
      const int src_y = source_rec->y + y;
      const int dest_x = dest_rec->x + x;
      const int dest_y = dest_rec->y + y;
      const int dest_r =
          (dest_y * dest->channels * dest->width + dest_x * dest->channels);
      const int src_r =
          (src_y * source->channels * source->width + src_x * source->channels);

      if (x >= dest_rec->width) {
        continue;
      }
      for (int c = 0; c < dest->channels; ++c) {
        stbi_uc pixel = (c < source->channels) ? source->data[src_r + c] : 255;
        dest->data[dest_r + c] = pixel;
      }
    }
  }
  return 0;
}
*/
