#include <stdlib.h>
#include "stb_rect_pack.h"
#include "stb_truetype.h"
#define FOXUI_STB_ALREADY_INCLUDED 1
#include "../foxui/foxui.h"


void foxui_configure_lnf(foxui_param* param)
{
  param->linegap = 0.6f;

  param->atlas_width = 1024.000000;
  param->atlas_height = 1024.000000;
  param->user_image_count = 11;
param->user_images = (foxui_rectangle*)foxui_malloc(sizeof(*param->user_images) * param->user_image_count);
param->user_images[0].x = 0.815430;
param->user_images[0].y = 0.049805;
param->user_images[0].width = 0.028320;
param->user_images[0].height = 0.028320;
param->user_images[1].x = 0.929688;
param->user_images[1].y = 0.000000;
param->user_images[1].width = 0.053711;
param->user_images[1].height = 0.049805;
param->user_images[2].x = 0.983398;
param->user_images[2].y = 0.000000;
param->user_images[2].width = 0.005859;
param->user_images[2].height = 0.041992;
param->user_images[3].x = 0.668945;
param->user_images[3].y = 0.049805;
param->user_images[3].width = 0.048828;
param->user_images[3].height = 0.049805;
param->user_images[4].x = 0.717773;
param->user_images[4].y = 0.049805;
param->user_images[4].width = 0.048828;
param->user_images[4].height = 0.049805;
param->user_images[5].x = 0.766602;
param->user_images[5].y = 0.049805;
param->user_images[5].width = 0.048828;
param->user_images[5].height = 0.049805;
param->user_images[6].x = 0.668945;
param->user_images[6].y = 0.000000;
param->user_images[6].width = 0.260742;
param->user_images[6].height = 0.049805;
param->user_images[7].x = 0.843750;
param->user_images[7].y = 0.049805;
param->user_images[7].width = 0.021484;
param->user_images[7].height = 0.021484;
param->user_images[8].x = 0.000000;
param->user_images[8].y = 0.000000;
param->user_images[8].width = 0.668945;
param->user_images[8].height = 0.853516;
param->user_images[9].x = 0.668945;
param->user_images[9].y = 0.099609;
param->user_images[9].width = 0.222656;
param->user_images[9].height = 0.001953;
param->user_images[10].x = 0.865234;
param->user_images[10].y = 0.049805;
param->user_images[10].width = 0.019531;
param->user_images[10].height = 0.019531;
  param->sdf_values[0]         = 0.309000;
  param->sdf_values[1]         = 0.082000;
param->lnf_list_count = 0;
param->lnf_list      = (foxui_widget_lnf*)malloc(sizeof(*param->lnf_list) * param->lnf_list_count);

}
