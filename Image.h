
#pragma once

#include <stdint.h>

typedef struct {
  int width;
  int height;
  /** Number of 8-bit component per pixel (e.g. 4 == RBA) */
  int channels;
  unsigned char *data;
} Image;

/** Load an image, return something < 0 in case of error. */
int Image_Load(Image *image, const char *path);
/** Create (allocate) a RGBA Image */
int Image_CreateRGBA(Image *image, const int width, const int height);
/** Import an image to OpenGl */
uint32_t Image_ToGLTexture(Image *image);


/** Copy part of a source image to the dest one. Images cannot overlap. If
 * `source_rec` is NULL, the whole image is copied. if `dest_rec` is smaller
 * than `source_rec` only this part of the image is copied. */
// TODO: Create a rectangle type
/*
int Image_Copy(Image *source, foxui_rectangle *source_rec, Image *dest,
               foxui_rectangle *dest_rec);
*/
